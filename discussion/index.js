// console.log("Hello World!")


// What are conditional statements?
	// Conditional statements allows us to control the flow of our program. It allows us to run a statement/instruction if the condition is met or run another separate instruction if otherwise.

// [SECTION] if, else if, and else statement

	let numA = -1;

	// if statement
		// Executes a statement if the specified condition is true.
	if (numA <0) {
		console.log("Hello")
	};

	/*

		Syntax:
			if(condition/s) {
				statement/instruction
			}


	*/

	// you can also check the condition if it's true or false by printing the result in the console.
	console.log(numA<0);

	// Re-assign the value of numA with 0
	numA = 0;

	// The if statement did not execute because the condition was not met.
	if (numA <0) {
		console.log("Hello again if numA is less than 0!")
	};

	// The condition results into false
	console.log(numA<0);

	let city = "New York";


	// we can also put strings in if statements
	if (city === "New York") {
		console.log("Welcome to New York City!");
	};

	console.log(city === "New York");

	// else if Clause

	/*
		- Executes a statement if the previous conditions are false and if the specifiec condition is true.
		- The else "if" clause is optional and can be added to capture additional conditions to change the flow of the program.
	*/


	let numB = 1;

	if(numA <0) {
		console.log("Hello");
	} else if (numB > 0) {
		console.log("World");
	};


	// If the if() condition was passed then else if() condition will not be read
	numA = 1;

	if(numA > 0) {
		console.log("Hello");
	} else if (numB > 0) {
		console.log("World");
	};

	city = "Tokyo";

	if(city === "New York") {
		console.log("Welcome to New York City!");
	} else if(city === "Tokyo") {
		console.log("Welcome to Tokyo, Japan!")
	}



	// else statement
	/*
		- Executes a statement if all other conditions are false.
		- The "else" statement is also optional cand can be added to capture any other result to change the flow of the program.
	*/

	if(numA === 0) { // the condition is false
		console.log("Hello");
	} else if(numB === 0) { // the condition is false
		console.log("World");
	} else  {
		console.log("Again");
	}

	// else if and else are not stand alone statements.

	// // this will result into an error
	// else if(numB === 0) { // the condition is false
	// 		console.log("World");
	// 	} else  {
	// 		console.log("Again");
	// 	}

	// This will result into an error
	// else  {
	// 		console.log("Again");
	// 	}




	// if, else if, and else statements with functions.

	/*	
		- Most of the times we would like to use if, else if and else statements with functions to control the flow of our application
		- By including them inside functions, we can decide when certain conditions will be checked instead of executing statement when JS loads.
	*/

	let message = "No Message";
	console.log(message);

	function determineTyphoonIntensity(windSpeed) {

		//conditions

		if (windSpeed < 30) {
			return "Not a typhoon yet.";
		} else if (windSpeed >= 31 && windSpeed <= 61) {
			return "Tropical Depression detected";
		} else if (windSpeed >= 62 && windSpeed <= 88) {
			return "Tropical storm detected";
		} else if (windSpeed >= 89 && windSpeed <= 177) {
			return "Severe Tropical Storm detected";
		} else {
			return "Typhoon detected.";
		};
	};

	message = determineTyphoonIntensity(32)
	console.warn(message);




// [SECTION] Truthy and Falsy

	/*
		- In JS a "truthy" value is a value that is considered true when encountered in a boolean context.
		- Value are considered true unless defined otherwise.
		- Falsey values:
			1. false
			2.  0
			3. ""
			4. null
			5. -0
			6. undefined
			7. NaN.
	*/

// Truthy Examples

	if(true) {
		console.log("Truthy")
	};

	if(1) {
		console.log("Truthy")
	};

	if ([]) {
		console.log("Truthy")
	};

	// let name = "France";

	// if(name) {
	// 	console.log("Truthy");
	// };

// Falsy Examples

	if(false) {
		console.log("Falsy")
	}

	if(0) {
		console.log("Falsy")
	}

	if(undefined) {
		console.log("Falsy")
	}

	if("") {
		console.log("Falsy")
	}

	if(null) {
		console.log("Falsy")
	}

	if(-0) {
		console.log("Falsy")
	}


	if(NaN) {
		console.log("Falsy")
	}

// [SECTION] Conditional (Ternary) Operator

	/*
		Syntax:
			(expression) ? ifTrue/Truthy : ifFalse/Falsy;
	*/

// Single Statement execution
	let ternaryResult = (1 < 18) ? true : false;
	console.log("Result of Ternary Operator: " + ternaryResult);

// Multiple Statement execution

	let name;

	function isOfLegalAge() {
		name = "John";
		return "You are of the legal age limit";
	};

	function isUnderAge(){
		name = "Jane";
		return "You are under the age limit";
	}

	// let age = parseInt(prompt("What is your age?"));
	// console.log(age);

	// let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
	// console.log("Result of Ternary Operator in functions: " + legalAge + ", " + name);

// [SECTION] Switch Statement

	/*
		- The switch statement evaluates an expressin and matches the expression's value to a case clause. The switch will then execute the statements associated with that matching case
		
		Syntax:
			switch(expression) {
	
			}
	*/

	// let day = prompt("What day of the week is it today?"). toLowerCase();

	// console.log(day);

	// switch(day) {
	// 	case 'monday':
	// 		console.log("The color of the day is red")
	// 		break;
	// 	case 'tuesday':
	// 		console.log("The color of the day is orange")
	// 		break;
	// 	case 'wednesday':
	// 		console.log("The color of the day is yellow")
	// 		break;
	// 	case 'thursday':
	// 		console.log("The color of the day is green")
	// 		break;
	// 	case 'friday':
	// 		console.log("The color of the day is blue")
	// 		break;
	// 	case 'saturday':
	// 		console.log("The color of the day is indigo")
	// 		break;
	// 	case 'sunday':
	// 		console.log("The color of the day is violet")
	// 		break;
	// 	default:
	// 		console.log("Please input a valid day.");
	// 		break;
	// }

// [SECTION] Try-Catch-Finally Statement

	function showIntensityAlert(windSpeed) {
		// try-catch-finally statement

		try{
			// alerat(determineTyphoonIntensity(windSpeed)); -error
			alert(determineTyphoonIntensity(windSpeed));
		} catch (error) {
			console.log(typeof error);
			console.warn(error.message);
		} finally {
			alert("Intensity updates will show new alerts.");
		};
	};


	showIntensityAlert(56);
	console.log("This message will still show since we handled our error properly.")